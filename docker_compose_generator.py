"""
This file generates a docker-compose.yml file based on secret configs placed stored
in the .env file. As such, any docker-compose.yml file should never be committed to git.

This file must be located in root directory of the repo.
"""

import os
from base_config import base_dir
from dotenv import load_dotenv
load_dotenv()

# Generate file path
filepath = os.path.join(base_dir, 'docker-compose.yml')
home_dir = os.path.expanduser('~')

# Get environment variables
postgres_host = os.getenv('POSTGRES_HOST')
postgres_db = os.getenv('POSTGRES_DB')
postgres_user = os.getenv('POSTGRES_USER')
postgres_password = os.getenv('POSTGRES_PASSWORD')

# Generate `docker-compose.yml` file from string
file_contents = f"""\
version: '2'
services:
    web:
        build: .
        ports:
            - "8000:8000"
        volumes:
            - .:/app
        links:
            - db

    db:
        image: "postgres:9.6"
        ports:
            - "5432:5432"
        environment:
            POSTGRES_USER: '{postgres_user}'
            POSTGRES_PASSWORD: '{postgres_password}'
            POSTGRES_DB: '{postgres_db}'
        volumes:
            - {os.path.join(os.path.expanduser('~'), 'postgres_docker', 'volumes', 'postgres')}:/var/lib/postgresql/data
"""

with open(filepath, 'w') as dc_file:
    dc_file.write(file_contents)

