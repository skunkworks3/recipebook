#!/bin/bash

# 1. Create a directory where the dockerized postgres database can persist its data on the host.
echo "1/3: Creating $HOME/postgres_docker/volumes/postgres"
mkdir -p $HOME/postgres_docker/volumes/postgres

# 2. Install python-dotenv
echo "2/3: pip installing python-dotenv"
python3 -m pip install python-dotenv

# 3. Generate the docker-compose.yml file
echo "3/3: Generating docker-compose.yml file"
python3 docker_compose_generator.py
