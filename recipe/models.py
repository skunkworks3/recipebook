from django.db import models
from django.core.validators import MaxLengthValidator
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.postgres.fields import JSONField
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from fractions import Fraction
import re

from recipe.parser import text_to_float, fraction_to_unicode, unicode_to_amount


class Item(models.Model):
    name = models.CharField(max_length=40, blank=True,
                            validators=[MaxLengthValidator(40)])
    plural_name = models.CharField(max_length=40, blank=True, default="",
                                   validators=[MaxLengthValidator(40)])
    ml_per_g = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    # ml per gram

    def display(self, plural):
        return self.plural_name or f"{self.name}s" if plural else self.name

    def __str__(self):
        return self.name


class Equipment(models.Model):
    name = models.CharField(max_length=40, blank=True,
                            validators=[MaxLengthValidator(40)])
    image = models.ImageField(upload_to='equipment/%Y/%m', null=True,
                              default=None, blank=True)

    def __str__(self):
        return self.name


class CookingStyle(models.Model):
    name = models.CharField(max_length=40, blank=True,
                            validators=[MaxLengthValidator(40)])
    equipment = models.ManyToManyField(Equipment, related_name='style',
                                       blank=True, default=None)

    def __str__(self):
        return self.name


class Measurement(models.Model):
    """
    Measurement scales.
    to_metric: is the conversion of mass to grams, volume to ml and
    temperature to C
    """
    VOL = 'V'
    MASS = 'M'
    TEMP = 'T'
    UNIT = 'U'
    TYPE_CHOICES = [(MASS, 'Mass'), (VOL, 'Volume'), (TEMP, 'Temperature'),
                    (UNIT, 'Unit')]
    name = models.CharField(max_length=16, blank=True,
                            validators=[MaxLengthValidator(16)])
    plural_name = models.CharField(max_length=16, blank=True, default="",
                                   validators=[MaxLengthValidator(16)])
    short = models.CharField(max_length=5, blank=True,
                             validators=[MaxLengthValidator(5)])
    fraction = models.BooleanField(default=False)
    type = models.CharField(max_length=1, blank=True, choices=TYPE_CHOICES,
                            default=MASS)
    cf_metric = models.DecimalField("Conversion Factor to Metric",
                                    max_digits=7,
                                    decimal_places=3,
                                    default=0)

    def display(self, plural):
        return self.plural_name or f"{self.name}s" if plural else self.name

    def to_celsius(self, amount):
        assert self.type == 'T', \
            "You can only convert a temperature to celsius"
        return (amount - 32) * 5/9

    def to_fahrenheit(self, amount):
        assert self.type == 'T', "You can only convert a temperature to " \
                                 "fahrenheit"
        return amount*9/5 + 32

    def to_metric(self, amount):
        if self.type == self.VOL:
            unit = "ml"
        elif self.type == self.MASS:
            unit = "g"
        else:
            unit = ""
        return f"{amount * self.cf_metric}{unit}"

    def __str__(self):
        return f"{self.name} ({self.type})"


class MeasurementLookup(models.Model):
    measure = models.ForeignKey(Measurement, on_delete=models.CASCADE)
    lookup = models.CharField(max_length=16)


class RegionTag(models.Model):
    name = models.CharField(max_length=40, validators=[MaxLengthValidator(40)])

    def __str__(self):
        return self.name


class MealTag(models.Model):
    name = models.CharField(max_length=40, validators=[MaxLengthValidator(40)])
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, default=None,
                               blank=True, null=True)

    def __str__(self):
        return self.name


class Recipe(models.Model):
    name = models.CharField(max_length=40, blank=True,
                            validators=[MaxLengthValidator(40)])
    regions = models.ManyToManyField(RegionTag, blank=True, default=None)
    meal_types = models.ManyToManyField(MealTag, related_name='recipe_type',
                                        blank=True, default=None)
    styles = models.ManyToManyField(CookingStyle, related_name='recipe',
                                    blank=True, default=None)
    recipe_yield = models.DecimalField(max_digits=6, decimal_places=2,
                                       default=0)
    yield_measure = models.ForeignKey(
        Measurement, on_delete=models.SET_NULL, blank=True, null=True,
        default=None)
    serving_count = models.PositiveSmallIntegerField(default=1)
    prep_time = models.PositiveSmallIntegerField(
        verbose_name='Preparation Time (min)', default=0)
    cook_time = models.PositiveSmallIntegerField(
        verbose_name='Cooking Time (min)', default=0)
    cool_time = models.PositiveSmallIntegerField(
        verbose_name='Cool Down Time (min)', default=0)
    information = MarkdownxField(blank=True, null=True, default=None)
    instruction = MarkdownxField(blank=True, null=True, default=None)
    data = JSONField(blank=True, null=True)

    def equipment_required(self):
        try:
            ae = self.data['additional_equipment']
            a = [str(x) for x in ([ae, ] if isinstance(ae, str) else ae)]
        except (KeyError, TypeError):
            a = []
        for style in self.styles.all():
            for x in style.equipment.all():
                a.append(str(x))
        return sorted(list(set(a)))

    def append_data(self, field, new):
        try:
            ae = self.data[field]
            self.data[field] = ae + new
        except TypeError:
            self.data = {field: new}
        except KeyError:
            self.data = {field: new}

    def display(self):
        equipment = self.equipment_required()
        text = f"# {self.name} ["\
            + ', '.join([f'{str(region)}' for region in self.regions.all()])
        if self.meal_types.count():
            text += f"]\n## Type of Meal: ["\
                + ", ".join([f'{str(meal)}'
                             for meal in self.meal_types.all()]) + ']'
        if self.serving_count and self.yield_measure:
            text += "\n    Yield: {} for {} {}".format(
                self.yield_measure.display(self.recipe_yield),
                self.serving_count, 'person' if self.serving_count == 1 else
                'people')
        text += "\n## Ingredients:\n"\
            + '\n'.join([f'* {str(i)}' for i in self.ingredient_set.all()])
        if equipment:
            text += f"\n## Equipment Required:\n" + '\n'.join(equipment)
        text += f"\n## Instructions:\n{self.instruction}"
        return markdownify(text)

    def parse_ingredient(self, text):
        p = re.compile('[0-9 /.]+')
        match = p.match(text)
        if not match:
            p = re.compile(r"\w+")
            match = p.match(text)
            amount = unicode_to_amount(match.group())
        else:
            amount = text_to_float(match.group())

        plural = amount > 1

        stuff = text[match.span()[1]:].strip().split(" ", 1)

        if len(stuff) == 2:
            measure_text = stuff[0][:-1] if plural else stuff[0]
            item_text = stuff[1]
        else:
            measure_text = ''
            item_text = stuff[0][:-1] if plural else stuff[0]

        if measure_text:
            try:
                measure = MeasurementLookup.objects.get(
                    lookup=measure_text.lower()).measure
            except ObjectDoesNotExist:
                measure = Measurement.objects.create(name=measure_text)
                MeasurementLookup.objects.create(measure=measure,
                                                 lookup=measure_text.lower())
        else:
            measure = None

        for item in Item.objects.all():
            if item.name in item_text:
                break
        else:
            item = Item.objects.create(name=item_text)

        return Ingredient.objects.create(
            recipe=self, amount=amount, measurement=measure, item=item)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=6, decimal_places=2, default=0)
    measurement = models.ForeignKey(Measurement, on_delete=models.SET_NULL,
                                    null=True, blank=True)
    item = models.ForeignKey(Item, on_delete=models.SET_NULL, null=True)

    def display_amount(self):
        whole = int(self.amount)
        part = self.amount - whole
        fraction = self.measurement.fraction if self.measurement else True

        if fraction:
            num_part = ""
            part = Fraction(part).limit_denominator(10)
            num_part += f"{whole}" if whole else ""
            num_part += f"{fraction_to_unicode(str(part))}" if \
                part.numerator else ""
        else:
            num_part = f"{self.amount}" if part else f"{whole}"
        return num_part

    def __str__(self):
        amount_str = self.display_amount()
        plural = self.amount > 1
        measure = self.measurement.display(plural) if self.measurement else ""
        plural = plural if not self.measurement else False
        item = self.item.display(plural)

        if self.measurement:
            return f"{amount_str} {measure} {item}"
        else:
            return f"{amount_str} {item}"


class Picture(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/%Y/%m/%d', null=True,
                              default=None, blank=True)
    data = JSONField(blank=True, null=True)

    def __str__(self):
        try:
            name = self.data['name']
        except KeyError:
            name = self.id
        return f"{self.recipe.name} ({name})"


class Video(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    url = models.URLField('Video Location')
    data = JSONField(blank=True, null=True)

    def __str__(self):
        try:
            name = self.data['name']
        except KeyError:
            name = self.id
        return f"{self.recipe.name} ({name})"
