"""
This utility, though helpful for getting started, will need to be implemented
client-side in order to auto-populate the 'Create New Recipe' form.
"""
import requests
from bs4 import BeautifulSoup
import re
from fractions import Fraction
from unicodedata import lookup


class UnicodeFraction:
    def __init__(self, code=r'\u00bc',  name='ONE FOURTH', value=1/4):
        self.code = code
        self.name = "VULGAR FRACTION " + name
        self.value = value

    def __str__(self):
        return self.name


VULGAR_FRACTION = {
    '1/4': UnicodeFraction("\u00bc", 'ONE FOURTH', 1/4),
    '1/2': UnicodeFraction("\u00bd", 'ONE HALF', 1/2),
    '3/4': UnicodeFraction("\u00be", 'THREE FOURTHS', 3/4),
    '1/7': UnicodeFraction("\u2150", 'ONE SEVENTH', 1/7),
    '1/9': UnicodeFraction("\u2151", 'ONE NINTH', 1/9),
    '1/10': UnicodeFraction("\u2152", 'ONE TENTH', 1/10),
    '1/3': UnicodeFraction("\u2153", 'ONE THIRD', 1/3),
    '2/3': UnicodeFraction("\u2154", 'TWO THIRDS', 2/3),
    '1/5': UnicodeFraction("\u2155", 'ONE FIFTH', 1/5),
    '2/5': UnicodeFraction("\u2156", 'TWO FIFTHS', 2/5),
    '3/5': UnicodeFraction("\u2157", 'THREE FIFTHS', 3/5),
    '4/5': UnicodeFraction("\u2158", 'FOUR FIFTHS', 4/5),
    '1/6': UnicodeFraction("\u2159", 'ONE SIXTH', 1/6),
    '5/6': UnicodeFraction("\u215A", 'FIVE SIXTHS', 5/6),
    '1/8': UnicodeFraction("\u215B", 'ONE EIGHTH', 1/8),
    '3/8': UnicodeFraction("\u215C", 'THREE EIGHTHS', 3/8),
    '5/8': UnicodeFraction("\u215D", 'FIVE EIGHTHS', 5/8),
    '7/8': UnicodeFraction("\u215E", 'SEVEN EIGHTHS', 7/8),
}


def text_to_float(text):
    num = 0.0
    if '/' in text:
        for item in text.split(" "):
            if '/' in item:
                num += float(Fraction(item))
            else:
                num += float(item) if item else 0
    else:
        num += float(text)
    return num


def fraction_to_unicode(text):
    return lookup(VULGAR_FRACTION[text].name)


def unicode_to_amount(code):
    for item, field in VULGAR_FRACTION.items():
        if field.code == code:
            return field.value
    return 0


def time_to_min(text):
    text = text.lower()
    p = re.compile('[0-9 /.]+')
    num_list = [match for match in p.finditer(text)]
    time = 0
    for i, item in enumerate(num_list):
        start = item.span()[1]
        end = num_list[i+1].span()[0] if i < (len(num_list) - 1) else len(
            text)
        time += text_to_float(item.group()) * (60 if 'h' in text[start:end]
                                               else 1)
    return time


def parse_time(soup, key):
    try:
        text = soup.find('time', itemprop=key)['datetime']
    except TypeError:
        return 0
    if not text:
        return 0

    time = 0
    p = re.compile('[0-9]+')
    for match in p.finditer(text):
        time += int(match.group()) * (60 if text[match.span()[-1]] == 'H'
                                      else 1)
    return time


class AllRecipesUrlParser:
    def __init__(self, url):
        assert "www.allrecipes.com" in url
        self.url = url
        self.response = requests.get(url)
        self.soup = BeautifulSoup(self.response.text, 'lxml')
        self.body = self.soup.find("body")
        self.title = self.soup.title.string.strip()
        try:
            self.description = self.soup.find(class_="submitter__description"
                                              ).string.strip()
        except AttributeError:
            self.description = ""
        self.prep_time = parse_time(self.soup, "prepTime")
        self.cook_time = parse_time(self.soup, "cookTime")
        self.total_time = parse_time(self.soup, "totalTime")
        # self.recipe_yield =
        # self.image_url_list = [self.soup.find(class_="rec-photo"
        #                                       ).attrs['src'],
        #                        image for image in self.soup.find(
        #         class_="photo-strip__items")]

        # print(self.get_results())

    def get_results(self):
        return f"""\
            'recipe_url': {self.url},
            'title': {self.title},
            'description': {self.description},
            'ingredient_list': {self.ingredient_list},
            'directions_list': {self.directions_list},
            'prep_time': {self.prep_time},
            'cook_time': {self.cook_time},
            'total_time': {self.total_time},
            'recipe_yield': {self.recipe_yield},
            'servings_count': {self.servings_count}"""
            # 'image_url_list': {self.image_url_list}

    def place_results(self, recipe):
        recipe.title = self.title
        recipe.information = self.description
        for thing in self.ingredient_list:
            recipe.parse_ingredient(thing)

        recipe.instruction = self.directions_list
# TODO  append image_url_list to 'data'
# todo  change instructions to markup
# todo  make a measurement merge to get rid of extra measurements

        recipe.prep_time = self.prep_time
        recipe.cook_time = self.cook_time
        recipe.total_time = self.total_time
        recipe.recipe_yield = self.recipe_yield or 0
        recipe.serving_count = self.servings_count or 1
        recipe.data.update({"scraped": True})
        recipe.save()

    # @property
    # def description(self):
    #     try:
    #         description_container = self.body.find('div', {
    #             'class', 'recipe-summary'})
    #         description = description_container.find('p', {
    #             'class', 'margin-0-auto'}).getText().strip()
    #         return description
    #     except KeyError:
    #         return None

    @property
    def ingredient_list(self):
        ingredients_section = self.body.find('ul', {
            'class': 'ingredients-section'})
        ingredients_containers = ingredients_section.findAll('li', {
            'class': 'ingredients-item'})
        ingredient_list = [container.find('span', {
            'class': 'ingredients-item-name'}).getText().strip()
                           for container in ingredients_containers]
        return ingredient_list

    @property
    def directions_list(self):
        directions_container_div = self.body.find('section', {
            'class', 'recipe-instructions'})
        directions_ul = directions_container_div.find('ul', {
            'class', 'instructions-section'})

        unsorted_directions_list = []

        for list_item in directions_ul.findAll('li', {
                'class', 'instructions-section-item'}):
            # extract Step Number
            step_number = list_item.find('span', {
                'class', 'checkbox-list-text'}).getText().strip()
            step_number = int(step_number.lstrip('Step ').strip())

            # extract Step Directions
            step_directions_container = list_item.find('div', {
                'class', 'section-body'})
            step_directions = step_directions_container.find(
                'p').getText().strip()

            # append step_number & step_directions to list
            unsorted_directions_list.append((step_number, step_directions))

        # sort directions by step_number & filter out step_number
        sorted_directions_list = [i[1] for i in sorted(
            unsorted_directions_list, key=lambda i: i[0])]
        return sorted_directions_list

    def get_data(self, search_key):
        try:
            recipe_info_section = self.body.find('aside', {
                'class', 'recipe-info-section'})
            recipe_meta_item_list = recipe_info_section.findAll('div', {
                'class', 'recipe-meta-item'})

            container = self._find_container_by_substring(
                recipe_meta_item_list, search_key)
            return container.find('div', {
                'class', 'recipe-meta-item-body'}).getText().strip()

        except ValueError:
            return None

    # @property
    # def prep_time(self):
    #     return self.get_data('prep')
    #
    # @property
    # def cook_time(self):
    #     return self.get_data('cook')
    #
    # @property
    # def total_time(self):
    #     return self.get_data('prep')

    @property
    def recipe_yield(self):
        return self.get_data('yield')  # I changed to lower case

    @property
    def servings_count(self):
        return self.get_data('servings')  # I changed to lower case

    # @property
    # def image_url_list(self):
    #     all_images_container = self.body.find('aside', {
    #         'class', 'primary-media-with-filmstrip'})
    #
    #     # Find center image
    #     center_image_div = all_images_container.find('div', {
    #         'class', 'image-container'})
    #     center_image_url = center_image_div.find('div', {
    #         'class', 'component'})['data-src']
    #
    #     # find extra images
    #     extra_img_filmstrip_div = all_images_container.find('div', {
    #         'class', 'image-filmstrip'})
    #     extra_img_container_list = extra_img_filmstrip_div.findAll('div', {
    #         'class', 'image-slide'})
    #
    #     # Add center_image to the list that we are building
    #     output_img_url_list = [center_image_url]
    #
    #     for idx, item in enumerate(extra_img_container_list):
    #         # The first image in this list is the 'add image' button; skip
    #         if idx == 0:
    #             continue
    #
    #         img_url = item.find('div', {'class', 'component'})['data-src']
    #
    #         # Append to img_url to list
    #         output_img_url_list.append(img_url)
    #
    #     return output_img_url_list

    @staticmethod
    def _find_container_by_substring(container_list, substring):
        # TODO: this needs better testing
        for item in container_list:
            if substring in item.getText():
                return item
        else:
            raise ValueError(f'Could not find {substring} in container_list')
