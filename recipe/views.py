from django.shortcuts import render
from django.http import HttpResponse
import subprocess


# Create your views here.
def index(request):
    return render(request, 'recipe/index.html')


def about(request):
    return render(request, 'recipe/about.html')


def login(request):
    # TODO: Show page if user is NOT logged in (otherwise redirect?)
    return render(request, 'recipe/login.html')


def signup(request):
    # TODO: Show page if user is NOT logged in (otherwise redirect?)
    return render(request, 'recipe/signup.html')


def reset_password(request):
    # TODO:
    return render(request, 'recipe/reset_password.html')


def logout(request):
    # TODO: Show page if user IS logged in (otherwise redirect?)
    return render(request, 'recipe/login.html')


def get_ip(request):
    """
    (For development) returns the ip of
    :param request:
    :return:
    """
    raw_ip_output = subprocess.run(['dig', '+short', 'myip.opendns.com', '@resolver1.opendns.com.'],
                                   capture_output=True)
    ip = raw_ip_output.stdout.strip()
    return HttpResponse(ip)
