from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('login/', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('reset_password/', views.reset_password, name='reset_password'),
    path('get_ip/', views.get_ip, name='ip'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


# For reference on how to render an image from media -
# {% for img in your_object %}
# <img src="{{ img.image.url }}" >
# {% endfor %}
#
# https://stackoverflow.com/questions/34563454/django-imagefield-upload-to-path
