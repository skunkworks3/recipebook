from django.contrib.admin.sites import AdminSite
from django.contrib.admin.options import (ModelAdmin,)
from django.contrib.auth.models import User
from django.urls import reverse
from django.test import TestCase, Client
from recipe.admin import *


class MockRequest:
    pass


class MockSuperUser:
    def has_perm(self, perm):
        return True


request = MockRequest()
request.user = MockSuperUser()


class ClassModelAdminTest(TestCase):
    def setUp(self):
        self.recipe = Recipe.objects.create(
            name="Best Chocolate Chip Cookies",
            data={"url": "https://www.allrecipes.com/recipe/10813/best"
                         "-chocolate-chip-cookies/"}
        )
        cup = Measurement.objects.create(
            name="cup", short='C', fraction=True, type='V')
        oz = Measurement.objects.create(
            name="ounce", short='oz', fraction=True, type='M')
        sugar = Item.objects.create(name='white sugar', ml_per_g=1.2)
        milk = Item.objects.create(name='milk', ml_per_g=1.0)
        egg = Item.objects.create(name='egg')
        butter = Item.objects.create(name='melted butter')


        self.client = Client()
        self.client.force_login(
            User.objects.create_superuser(username='test', password='test'))
        self.site = AdminSite()

    def test_modeladmin_str(self):
        ma = ModelAdmin(Recipe, self.site)
        self.assertEqual(str(ma), 'recipe.ModelAdmin')

    def test_default_fields(self):
        fields = ['name', 'regions', 'meal_types', 'styles', 'recipe_yield',
                  'yield_measure', 'serving_count', 'prep_time', 'cook_time',
                  'cool_time', 'information', 'instruction', 'data']
        ma = ModelAdmin(Recipe, self.site)
        self.assertEqual(list(ma.get_form(request).base_fields), fields)
        self.assertEqual(list(ma.get_fields(request)), fields)
        self.assertEqual(list(ma.get_fields(request, self.recipe)), fields)
        self.assertIsNone(ma.get_exclude(request, self.recipe))

    def test_review_website(self):
        change_url = reverse('admin:recipe_recipe_changelist')
        data = {'action': 'review', '_selected_action': [self.recipe.id, ]}
        response = self.client.post(change_url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        recipe = Recipe.objects.get(id=self.recipe.id)
        self.assertEqual(
            recipe.data["review"][200:266],
            "'ingredient_list\': [\'1 cup butter, softened\', \'1 cup white "
            "sugar\',")

    def test_scrape(self):
        change_url = reverse('admin:recipe_recipe_changelist')
        data = {'action': 'scrape', '_selected_action': [self.recipe.id, ]}
        response = self.client.post(change_url, data, follow=True)
        self.assertEqual(response.status_code, 200)
        recipe = Recipe.objects.get(id=self.recipe.id)
        # self.assertEqual(recipe.information, 'Crisp edges, chewy middles.')
        self.assertTrue(recipe.data['scraped'])
        self.assertEqual(recipe.serving_count, 1)
        self.assertEqual(recipe.instruction[:20], "['Preheat oven to 35")
        self.assertEqual(recipe.ingredient_set.count(), 11)


    # def test_get_lookup(self):
    #     change_url = reverse('admin:recipe_measurement_changelist')
    #     data = {'action': 'create_lookup', '_selected_action': [m.id, ]}
    #     response = self.client.post(change_url, data, follow=True)
    #     self.assertEqual(response.status_code, 200)
    #     m = Measurement.objects.get(id=self.cup.id)
    #     self.assertEqual(2, m.measurementlookup_set.count())
    #     m = Measurement.objects.create(name="tablespoon")
    #     data = {'action': 'create_lookup', '_selected_action': [m.id, ]}
    #     response = self.client.post(change_url, data, follow=True)
    #     self.assertEqual(response.status_code, 200)
    #     m = Measurement.objects.get(id=m.id)
    #     self.assertEqual(1, m.measurementlookup_set.count())

    # def test_combine_duplicate(self):
    #
    #     MeasurementLookup.objects.create(lookup='cups', measure=m1)
    #     m2 = Measurement.objects.create(name="cup", type='V')
    #     MeasurementLookup.objects.create(lookup='cup', measure=m2)
    #     m3 = Measurement.objects.create(name='')
    #     self.assertEqual(Measurement.objects.count(), 3)
    #
    #     change_url = reverse('admin:recipe_measurement_changelist')
    #     data = {'action': 'combine_duplicates', '_selected_action': [
    #         m1.id, m2.id, m3.id]}
    #     response = self.client.post(change_url, data, follow=True)
    #


