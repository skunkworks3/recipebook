from django.test import TestCase
from recipe.parser import *


class ParserTest(TestCase):
    def test_time_to_min(self):
        self.assertEqual(time_to_min('15 min'), 15)
        self.assertEqual(time_to_min('15m'), 15)
        self.assertEqual(time_to_min('15'), 15)
        self.assertEqual(time_to_min('1 minute'), 1)
        self.assertEqual(time_to_min('2 minutes'), 2)
        self.assertEqual(time_to_min('1/2 hr'), 30)
        self.assertEqual(time_to_min('1 1/2 hr'), 90)
        self.assertEqual(time_to_min('3'), 3)
        self.assertEqual(time_to_min('.5h'), 30)
        self.assertEqual(time_to_min('1.5h'), 90)
        self.assertEqual(time_to_min('1hr 15 min'), 75)
        self.assertEqual(time_to_min('30 min 2 hr'), 150)
        self.assertEqual(time_to_min('min 15'), 15)
        self.assertEqual(time_to_min(''), 0)

    def test_place_results(self):
        return

    def test_all_recipes(self):
        url = "https://www.allrecipes.com/recipe/278882/triple-chocolate-chunk-cookies"
        a = AllRecipesUrlParser(url)
        self.assertEqual(a.title, 'Triple Chocolate Chunk Cookies Recipe - Allrecipes.com')
        self.assertEqual(a.prep_time, 15)
        self.assertEqual(a.cook_time, 10)
        self.assertEqual(a.total_time, 85)
        self.assertEqual(a.description[1:15], 'Large or small')
