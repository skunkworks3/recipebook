from django.test import TestCase
from recipe.models import *


def create_recipe():
    return Recipe.objects.create(name='Tiny Bubbles')


class ModelTest(TestCase):
    def test_create_item(self):
        name = 'White Sugar'
        ml_per_g = 1.2
        it = Item.objects.create(name=name, ml_per_g=ml_per_g)
        self.assertTrue(isinstance(it, Item))
        self.assertEqual(str(it), name)
        self.assertEqual(it.ml_per_g, ml_per_g)

    def test_create_equipment(self):
        name = 'Large Pot'
        it = Equipment.objects.create(name=name)
        self.assertTrue(isinstance(it, Equipment))
        self.assertEqual(str(it), name)

    def test_create_style(self):
        name = 'Double Boiler'
        it = CookingStyle.objects.create(name=name)
        it.equipment.add(Equipment.objects.create(name="Large Pot"))
        it.equipment.add(Equipment.objects.create(name="Small Pot"))

        self.assertTrue(isinstance(it, CookingStyle))
        self.assertEqual(str(it), name)
        self.assertEqual(it.equipment.count(), 2)

    def f_to_c_conversion(self, f, c_expected):
        """
        Tests Fahrenheit to Celsius conversion.
        f: test fahrenheit temperature
        c_expected: expected result in celsius
        :return:
        """
        c_calculated = Measurement(
            name='Degrees Fahrenheit', type='T', cf_metric=0).to_celsius(f)
        self.assertEqual(c_calculated, c_expected,
                         f"""
                        Expected: {f}F == {c_expected}C
                        Received: {f}F == {c_calculated}C
                        """)

    def c_to_f_conversion(self, c, f_expected):
        """
        Tests Celsius to Fahrenheit conversion.
        c: test celsius temperature
        f_expected: expected result in Fahrenheit
        :return:
        """
        f_calculated = Measurement(
            name='Degrees Celsius', type='T', cf_metric=0).to_fahrenheit(c)
        self.assertEqual(f_calculated, f_expected,
                         f"""
                        Expected: {c}C == {f_expected}F
                        Received: {c}C == {f_calculated}F
                        """)

    def test_measurement(self):
        self.f_to_c_conversion(32, 0)
        self.f_to_c_conversion(212, 100)
        self.c_to_f_conversion(0, 32)
        self.c_to_f_conversion(100, 212)
        amount = 1.66
        cf_metric = 236.6
        measurement = Measurement(name='cup', short='C', type='V',
                                  fraction=True, cf_metric=cf_metric)
        self.assertTrue(isinstance(measurement, Measurement))
        self.assertEqual(measurement.to_metric(amount),
                         f"{amount*cf_metric}ml")
        self.assertEqual(measurement.display(True), "cups")
        self.assertEqual(measurement.display(False), "cup")
        # self.assertEqual(measurement.display(amount), "1\u2154 C")

    def test_create_region(self):
        name = 'Portugal'
        it = RegionTag.objects.create(name=name)
        self.assertTrue(isinstance(it, RegionTag))
        self.assertEqual(str(it), name)

    def test_create_meal(self):
        name = 'Dessert'
        it = MealTag.objects.create(name=name)
        self.assertTrue(isinstance(it, MealTag))
        self.assertEqual(str(it), name)
        cookie = MealTag.objects.create(name='Cookie', parent=it)
        self.assertEqual(name, str(cookie.parent))

    def test_ingredient(self):
        return  # Todo Test adding Ingredients to recipes

    def test_recipe(self):
        recipe = create_recipe()
        style = CookingStyle.objects.create(name="Suds and Lather")
        style.equipment.add(Equipment.objects.create(name="Straw"))
        style.equipment.add(Equipment.objects.create(name="Water Jet"))
        recipe.styles.add(style)
        recipe.recipe_yield = 1.66
        cf_metric = 236.6
        recipe.yield_measure = Measurement(name='Cup', short='C', type='V',
                                           fraction=True, cf_metric=cf_metric)
        recipe.serving_count = 8
        return  # Todo Test creating a recipe

    def test_equipment_required(self):
        recipe = create_recipe()
        self.assertTrue(isinstance(recipe, Recipe))
        self.assertEqual(recipe.equipment_required(), [],
                         "Empty Equipment List Not Processed Correctly")
        style = CookingStyle.objects.create(name="Suds and Lather")
        style.equipment.add(Equipment.objects.create(name="Straw"))
        style.equipment.add(Equipment.objects.create(name="Water Jet"))
        recipe.styles.add(style)
        self.assertEqual(recipe.equipment_required(), [
            'Straw', 'Water Jet'])
        recipe.append_data("additional_equipment", "Red Spatula")
        self.assertEqual(recipe.equipment_required(), [
            'Red Spatula', 'Straw', 'Water Jet'])
        recipe.data = {"Creator": "Randy"}
        recipe.append_data("additional_equipment", [
            "Red Spatula", 'Xerox', 'Straw'])
        self.assertEqual(recipe.equipment_required(), [
            'Red Spatula', 'Straw', 'Water Jet', 'Xerox'])

    def test_parse_ingredient(self):
        recipe = create_recipe()
        ingredient_list = [
            '1 cup butter, softened', '1 cup white sugar',
            '1 cup packed brown sugar', '2 eggs',
            '2 teaspoons vanilla extract', '1 teaspoon baking soda',
            '1 teaspoons hot water', '3 cups all-purpose flour',
            '2 cups semisweet chocolate chips', '1 cup chopped walnuts']
        for item in ingredient_list:
            self.assertEqual(str(recipe.parse_ingredient(item)), item)

        item = '\u00bd teaspoon salt'
        measure = Measurement.objects.get(name='teaspoon')
        measure.fraction = True
        measure.type = 'V'
        measure.save()
        self.assertEqual(str(recipe.parse_ingredient(item)), item)

        butter = Ingredient.objects.get(item=Item.objects.filter(
            name__contains="butter")[0])
        flour = Ingredient.objects.get(item=Item.objects.filter(
            name__contains="flour")[0])
        chips = Ingredient.objects.get(item=Item.objects.filter(
            name__contains="chips")[0])
        self.assertEqual(butter.measurement, flour.measurement)
        self.assertEqual(butter.measurement, chips.measurement)
        self.assertEqual(flour.measurement.display(False), "cup")
        vanilla = Ingredient.objects.get(item=Item.objects.filter(
            name__contains="vanilla")[0])
        soda = Ingredient.objects.get(item=Item.objects.filter(
            name__contains="soda")[0])
        self.assertEqual(vanilla.measurement, soda.measurement)
        self.assertEqual(vanilla.measurement.display(False), "teaspoon")
