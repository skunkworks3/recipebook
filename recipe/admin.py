from django.contrib import admin
# from markdownx.admin import MarkdownxModelAdmin
from .models import *
from .parser import AllRecipesUrlParser

admin.site.register(Item)
admin.site.register(Equipment)
admin.site.register(CookingStyle)
admin.site.register(RegionTag)
admin.site.register(MealTag)
admin.site.register(Picture)
admin.site.register(Video)
# admin.site.register(Recipe, MarkdownxModelAdmin)


class IngredientInline(admin.TabularInline):
    model = Ingredient
    fields = ('amount', 'measurement', 'item')
    extra = 1


class MeasurementLookupInline(admin.TabularInline):
    model = MeasurementLookup
    fields = ('lookup', )
    extra = 1


class PictureInline(admin.TabularInline):
    model = Picture
    fields = ('image', 'data')
    extra = 1


class VideoInline(admin.TabularInline):
    model = Video
    fields = ('url', 'data')
    extra = 1


@admin.register(Measurement)
class MeasurementAdmin(admin.ModelAdmin):
    actions = ['create_lookup', 'combine_duplicates']
    inlines = (MeasurementLookupInline, )

    def create_lookup(self, obj, queryset):
        for item in queryset.all():
            if item.measurementlookup_set.count() < 2:
                MeasurementLookup.objects.get_or_create(
                    measure=item, lookup=item.name.lower())
                if item.short:
                    MeasurementLookup.objects.get_or_create(
                        measure=item, lookup=item.short.lower())

    def combine_duplicates(self, obj, queryset):
        if queryset.objects.count() < 2:
            return
        keep = None
        count = 0
        for item in queryset.objects.all():
            if item.measurementlookup_set.count() > count:
                keep = item
                count = item.measurementlookup_set.count()
        keep = keep or queryset.objects.all()[0]  # take the first one
        for item in queryset.objects.all():
            if item == keep:
                continue
            keep.fraction = keep.fraction or item.fraction
            keep.cf_metric = keep.cf_metric or item.cf_metric
            keep.short = keep.short or item.short
            if item.type != 'M':
                keep.type = item.type
            keep.save()
            for ingredient in item.ingredient_set.all():
                ingredient.measurement = keep
            for lookup in item.measurementlookup_set.all():
                if not lookup:
                    continue
                try:
                    MeasurementLookup.objects.get(measure=keep, lookup=lookup)
                except ObjectDoesNotExist:
                    lookup.measure = keep
                    lookup.save()
            item.delete()


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ('name', 'url_exists')
    list_filter = ('regions', 'meal_types')
    readonly_fields = ('equipment_required', 'display')
    fields = ('name', ('regions', 'meal_types', 'styles'),
              ('information', 'equipment_required'),
              ('prep_time', 'cook_time', 'cool_time'),
              ('serving_count', 'recipe_yield', 'yield_measure', ),
              'data', 'instruction', 'display')
    actions = ['review', 'scrape', ]
    inlines = (IngredientInline, PictureInline, VideoInline)

    def url_exists(self, item):
        try:
            return True if item.data['url'] else False
        except KeyError:
            return False
        except TypeError:
            return False
    url_exists.boolean = True

    def review(self, obj, queryset):
        for item in queryset.all():
            try:
                url = item.data['url']
            except KeyError:
                continue
            try:
                item.data["review"]
            except KeyError:
                if "allrecipes.com" in url:
                    item.data["review"] = AllRecipesUrlParser(url).get_results()
                    item.save()
    review.short_description = "Review from Website"

    def scrape(self, obj, queryset):
        for item in queryset.all():
            try:
                url = item.data['url']
            except KeyError:
                continue
            try:
                if item.data['scraped']:
                    return
            except KeyError:
                pass
            if "allrecipes.com" in url:
                AllRecipesUrlParser(url).place_results(item)
    scrape.short_description = "Copy from Website"
