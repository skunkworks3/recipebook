#!/bin/bash 

echo "Deleting existing persistent DB files"
sudo rm -rf ~/postgres_docker/

echo "Recreating persistent DB files"
mkdir -p ~/postgres_docker/volumes/postgres

echo "done"
