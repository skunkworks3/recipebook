"""
This file holds useful variables that pertain to the entire app.
As such, it must remain in the root directory of the repo.
"""
import os

# Get the base_dir path
base_dir = os.path.abspath(os.path.dirname(__file__))
print(base_dir)
