recipebook 
[![pipeline status](https://gitlab.com/skunkworks3/recipebook/badges/master/pipeline.svg)](https://gitlab.com/skunkworks3/recipebook/-/commits/master)
[![coverage report](https://gitlab.com/skunkworks3/recipebook/badges/master/coverage.svg)](https://gitlab.com/skunkworks3/recipebook/-/commits/master)
====

A cool web app for managing recipes.